#include <stdio.h>
#include <stdlib.h>
struct node{
    int data;
    struct node * next;
};
void traversal(struct node * ptr){
    while(ptr!=NULL)
 { printf("Elements = %d\n",ptr->data);
    ptr=ptr->next;
 }
}
struct node * insertfirst(struct node *head, int data){
    struct node * ptr = (struct node*)malloc(sizeof(struct node));
    ptr->next = head;
    ptr->data = data;
    return ptr;
}
struct node * insert_at_index(struct  node *head, int data, int index){
    struct node * ptr = (struct node*)malloc(sizeof(struct node));
    struct node * p = head;
    int i = 0;
    while(i!=index-1){
        p=p->next;
        i++;
    }
    ptr->next=p->next;
    p->next=ptr;
    ptr->data=data;
    return head;
}
struct node * insert_at_end(struct node * head, int data){
	struct node * ptr = (struct node*)malloc(sizeof(struct node));
	ptr->data=data;
	struct node * p = head;
	while(p->next==NULL){
	p = p->next;
	}
	p->next = ptr;
	ptr->next = NULL;
	return head;
}
struct node * Insert_after_node(struct node* head, struct node * prevnode, int data){
	struct node * ptr =(struct node *)malloc(sizeof(struct node));
	ptr->data=data;
	ptr->next=prevnode->next;
	prevnode->next=ptr;
	return head;
}
	
int main(){
    struct node * head;
    struct node * second;
    struct node * third;
    struct node * fourth;
    head = (struct node*)malloc(sizeof(struct node));
    second = (struct node*)malloc(sizeof(struct node));
    third = (struct node*)malloc(sizeof(struct node));
    fourth = (struct node*)malloc(sizeof(struct node));
    head->data = 8;
    head->next = second;

    second->data = 12;
    second->next = third;

    third->data = 16;
    third->next = fourth;

    fourth->data = 20;
    fourth->next = NULL;
	
	head=Insert_after_node(head,second,32);
	//head = insert_at_end(head,24);
    head=Insert_after_node(head,fourth,36);
   
    //head=insertfirst(head,4);
    //printf("linked list after insertion at beginning\n");
   printf("linked list after inserting in between\n");
  //  head=insert_at_index(head,14,1);
  //  traversal(head);
    traversal(head);
 
 

return 0;
}