#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node * next;
};

void display(struct node * head){
	struct node * ptr = head;
	do{
		printf("%d\n",ptr->data);
		ptr=ptr->next;
	}
	while(ptr->next != head);
	printf("%d\n",ptr->data);
}

struct node * insert_first(struct node * head, int data){
	struct node * ptr = (struct node*)malloc(sizeof(struct node));
	ptr->data = data;
	struct node *p = head->next;
	while(p->next!=head){
	p=p->next;}
	p->next = ptr;
	ptr->next = head;
	head = ptr;
	return head;
}
	
struct node * insert_last(struct node * head, int data){
	struct node * ptr = (struct node*)malloc(sizeof(struct node));
	ptr->data = data;
	struct node *p = head->next;
	while(p->next!=head){
		p=p->next;
	}
	p->next=ptr;
	ptr->next=head;
	return head;
}

struct node * delete_first(struct node * head){
	struct node *p = head;
	while(p->next!=head){
		p=p->next;
	}
	p->next=head->next;
	free(head);
	head = p->next;
	
	
	return head;
}

struct node * delete_last(struct node * head){
	struct node *p = head;
	struct node *ptr;
	while(p->next!=head){
		ptr = p;
		p=p->next;
	}
	ptr->next = p->next;
	free(p);
	return head;
}
  	
    	
	
int main(){
	struct node * head;
	struct node * second;
	struct node * third;
	struct node * fourth;
	head=(struct node*)malloc(sizeof(struct node));
    second=(struct node*)malloc(sizeof(struct node));
	third=(struct node*)malloc(sizeof(struct node));
	fourth=(struct node*)malloc(sizeof(struct node));
	
	head->data = 5;
	head->next = second;
	second->data = 7;
	second->next = third;
	third->data = 9;
	third->next = fourth;
	fourth->data = 11;
	fourth->next = head;
	
	display(head);
	printf("linked list after insetion \n");
	//head=insert_first(head,13);
	//head = insert_last(head,23);
	//head = delete_first(head);
	head = delete_last(head);
	display(head);
	
	
	
	
	return 0;
}
	