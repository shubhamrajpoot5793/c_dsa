#include<iostream>
using namespace std;
template <typename T> T myMax(T x , T y)
{
    return (x > y) ? x : y;
}
int main(){
    cout<<myMax(8,4)<<endl;
    cout<<myMax(4.7,8.9)<<endl;
    cout<<myMax('g','k')<<endl;
    return 0;
}