#include<stdio.h>
#include<stdlib.h>

struct node{
	int data;
	struct node * next;
};
void traversal(struct node * ptr){
	while(ptr!=NULL){
		printf("element = %d\n",ptr->data);
		ptr=ptr->next;
	}
}
//for deleting first element of linked list
struct node * delete_first(struct node* head){
	struct node * ptr = head;
	head = head->next;
	free(ptr);
	return head;
}
//deleting element at given index
struct node * delete_index(struct node* head, int index){
	struct node * p = head;
	struct node * q = head->next;
	for(int i = 0; i < index-1; i++);
	{
		p=p->next;
		q=q->next;
	}
	p->next=q->next;
	free(q);
	return head;
}
//deleting element at last node
struct node * delete_end(struct node * head){
	struct node * p = head;
	struct node * q = head->next;
	while(q->next!=NULL){
		p=p->next;
		q=q->next;
	}
	p->next==NULL;
	free(q);
	return head;
}
//deleting elements with  given value
struct node * delete_index_given(struct node* head, int value){
	struct node * p = head;
	struct node * q = head->next;
	while(q->data!=value && q->next != NULL)
	{
		p=p->next;
		q=q->next;
	}
	if(q->data == value){
		p->next = q->next;
		free(q);
	}
	
	return head;
}

int main(){
	struct node * head;
	struct node * second;
	struct node * third;
	struct node * fourth;
	head=(struct node*)malloc(sizeof(struct node));
    second=(struct node*)malloc(sizeof(struct node));
	third=(struct node*)malloc(sizeof(struct node));
	fourth=(struct node*)malloc(sizeof(struct node));
	
	head->data=4;
	head->next=second;
	second->data=8;
	second->next=third;
	third->data=12;
	third->next=fourth;
	fourth->data=16;
	fourth->next=NULL;
	
	printf("linked list before deletion \n");
	traversal(head);
	//head = delete_first(head);
	//head = delete_index(head,2);
	//head = delete_end(head);
	head = delete_index_given(head,12);
	printf("linked list after deletion \n");
	traversal(head);
	return 0;
}
	
