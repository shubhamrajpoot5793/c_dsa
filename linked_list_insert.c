#include <stdio.h>
#include <stdlib.h>
struct node{
    int data;
    struct node * next;
};
void traversal(struct node * ptr){
    while(ptr!=NULL)
 { printf("Elements = %d\n",ptr->data);
    ptr=ptr->next;
 }
}
struct node * insertfirst(struct node *head, int data){
    struct node * ptr = (struct node*)malloc(sizeof(struct node));
    ptr->next = head;
    ptr->data = data;
    return ptr;
}
int main(){
    struct node * head;
    struct node * second;
    struct node * third;
    struct node * fourth;
    head = (struct node*)malloc(sizeof(struct node));
    second = (struct node*)malloc(sizeof(struct node));
    third = (struct node*)malloc(sizeof(struct node));
    fourth = (struct node*)malloc(sizeof(struct node));
    head->data = 8;
    head->next = second;

    second->data = 12;
    second->next = third;

    third->data = 16;
    third->next = fourth;

    fourth->data = 20;
    fourth->next = NULL;

    traversal(head);
    head=insertfirst(head,4);
    printf("linked list after insertion at beginning\n");
    traversal(head);

return 0;
}